# CmDock PyMOL Plugin

## Description
The CmDock PyMOL plugin is a PyMOL GUI tool that combines the visualization capabilities of the PyMOL molecular docking software with CmDock. It provides a graphical user interface to use CmDock and easily visualize the results. 

## Installation
In PyMOL install the plugin in the plugins tab:
- With Plugin -> Plugin Manager -> Install New Plugin -> Install from local file select the CmD_Plugin.py file
- Upon first startup of the plugin input location of the .CmD_Plugin directory


## Citation
In development.

## Project status
In development.
