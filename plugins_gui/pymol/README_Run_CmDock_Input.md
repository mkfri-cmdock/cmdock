# Integration with Run_CmDock_input.py script:

Run_CmDock_input.py (located in .CmD_plugin/modules) is a script for the automated mass receptor file and cavity preparation, as well as mass docking with ligands.
The script has the following input options:
-	-i  the input file
-	-d the directory where it will write the results
-	-l the file where it will write the log
-	-n the number of simultaneous processors used (with python multiprocessing)
-	-p the folder where it will download pdb files (if they are required)


The input file is formated as a configuration .ini file (read with the python cofigparser library, https://docs.python.org/3/library/configparser.html).
A basic configuration file for  receptor preparation and cavity formation may look like this:

# Input file examples

     [DEFAULT]     
          #the special default section, provides default values to the other sections

          radius = 9     
          small_sphere = 1.5     
          large_sphere = 5 
          CmCavity_exe = cmcavity
          

     [1OG2_A]     
          # this is a section

          PDB_ID = 1OG2     
          ligands_kept = HEC_501_A     
          chains_kept = A      
          two_sphere_center = 6.0, 75.0, 29.0 


     [1OG5_A]     

          PDB_ID = 1OG5     
          ligands_kept = HEC_501_A     
          chains_kept = A      
          reference_ligand = SWF_502_A


It is comprised of sections with headers in square brackets [section]. The script will use the name of the section as the name of the receptor. 
In this example we have two sections 1OG2 and 1OG5. We define the **PDB_ID** for each of the sections, the script will download the corresponding .pdb files from the RCSB PDB. 
We specifi which ligands and chains we want to keep in the resulting receptor file through the **chains_kept** and  **ligands_kept** parameters. 
We also specify the method of cavity preparation, the two sphere method by providing the **two_sphere_center** parameter or the reference ligand through the **reference_ligand** parameter.

A special section is the [DEFAULT]  section, this provides the default values for all the other section. These values can be overwritten from within a specific section.
In this case we define parameters for cavity creation, the radius (**radius**), small sphere and large sphere (**small_sphere**, **large_sphere**) values and the executable with which to call for CmCavity (**CmCavity_exe**).

An input file for launching molecular docking might look something like this:


     [DEFAULT]     
          #the special default section, provides default values to the other sections

          CmDock_exe = cmdock
          n_docking_runs = 100
          n_best_poses = 5
          dock_score_parameter = score.inter
          main_results_file = Docking_results.dat
          

     [1OG2_A]     
          prm_file =  path/to/database/1OG2_A_two_sphere.prm
          ligand_dat  = ligands_1OG2.dat
          


     [1OG5_A]     
          prm_file =  path/to/database/1OG5_A_ref_lig.prm
          ligand_dat  = ligands_1OG5.dat


Here we provide in the [DEFAULT] section the executable to call for CmDock (CmDock_exe), the number of docking runs (n_docking_runs, -n parameter),  the number of best poses kept (n_best_poses, -b parameter),  the name main results file file where the score of the best scoring poses will be recorded (main_results_file), and the score parameter which to use when looking for the best score (dock_score_parameter).

In each of the sections we provide the location of the .prm file (prm_file) and the location of a file with the paths to ligands we wish to dock the receptor with (ligand_dat).
We could also join the cavity creation and docking into a single input, by adding the docking parameters from the default section and the ligand_dat entries to the receptor preparation section. This would then make the cavities and run the docking.


# Description of the possible parameters in the input file:

## Parameters for Receptor preparation:

**PDB_ID** 		- the PDB id of the receptor, will download the .pdb file from the RCSB PDB

**PDB_file**		- the path to the .pdb file, will ignore **PDB_ID** if set

**chains_kept**	- The chains to keep in the receptor file (seperated by ,)

**ligands_kept**	- the ligands to keep in the receptor file (seperated by ,), in the format of 
ResidueName_ResidueNumber_ChainIdenrifier (eg. HEC_501_A)

**receptor_file**	- the finished receptor file, if provided will use this file and ignore **PDB_file**, **chains_kept**, **ligands_kept** and  **PDB_ID**

**subdir**		- the subdirectory in which to write the files, by default the section name


## Parameters for Cavity creation:

**radius** 		- the radius of the cavity, defaults to 10 if not provided

**small_sphere**	- the radius of the small sphere, defaults to 1.5 

**max_cavities**	- the maximum number of cavities, defaults to 1

**vol_increase**	- the volume increase, defaults to 0

**grid_step** 		- the grid step, defaults to 0.5

**large_sphere**	- the radius of the large sphere, used for two sphere method

**CmCavity_exe** 	- the command to execute to access CmCavity

**two_sphere_center**	- the two sphere method of cavity creation, coordinates of the center

**reference_ligand**	- the reference ligand method of cavity creation. If the provided is the path to a file it will use the file as the reference ligand, otherwise will try to extract the ligand from the pdb file reference ligand is defined as  ResidueName_ResidueNumber_ChainIdenrifier (eg. SWF_502_A)
prm_file		- the finished prm file to use in cavity creation, will ignore everything else in cavity and receptor preparation


## Parameters for Molecular Docking:

**n_docking_runs** 	- number of docking runs, -n 

**n_best_poses**		- number of best poses kept, -b switch (will not use -b if negative)

**ligand_dat**		- The path to a text file containing paths to .sdf ligand files to dock with

**dock_score_parameter**	- the score parameter used for finding the best score, score.norm by default

**main_results_file**	- the file in which to write the scores of the best scoring poses

**CmDock_exe** 		- the command to execute to access CmDock
















