# Copyright Notice
# ================
#
# The PyMOL Plugin source code in this file is copyrighted, but you can
# freely use and copy it as long as you don't change or remove any of
# the copyright notices.
#
# ----------------------------------------------------------------------
# This PyMOL Plugin is Copyright (C) 2022 by Marko Jukic <marko.jukic@um.si>
#
#                        All Rights Reserved
#
# Permission to use, copy and distribute
# versions of this software and its documentation for any purpose and
# without fee is hereby granted, provided that the above copyright
# notice appear in all copies and that both the copyright notice and
# this permission notice appear in supporting documentation, and that
# the name(s) of the author(s) not be used in advertising or publicity
# pertaining to distribution of the software without specific, written
# prior permission.
#
# THE AUTHOR(S) DISCLAIM ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
# INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS.  IN
# NO EVENT SHALL THE AUTHOR(S) BE LIABLE FOR ANY SPECIAL, INDIRECT OR
# CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF
# USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE
# OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
# PERFORMANCE OF THIS SOFTWARE.
# ----------------------------------------------------------------------


# -*- coding: utf-8 -*-
# CmDock plugin
# written for python 3.8.x
# Not Yet Described at PyMOL wiki: /
# Author : Marko Jukic
# Date: 2022
# License: UM FKKT Laboratory of Physical Chemistry and Chemical Thermodynamics


#
from __future__ import absolute_import
from __future__ import print_function
from genericpath import isfile
import sys
import re
import gzip
import os
import os.path as op
from typing import Text
import urllib.request
import urllib.parse
import urllib.error
import urllib.request
import urllib.error
import urllib.parse
import subprocess
from matplotlib.cbook import ls_mapper
import numpy as np

import configparser
import pymol
from pymol.cgo import *
from pymol import cmd
from pymol import stored
from pymol.vfont import plain

import time
from glob import glob
from ftplib import FTP
from pathlib import Path

from pymol.Qt import QtWidgets, QtGui
from pymol.Qt.utils import loadUi

Plugin_Name_ = "CmD_plugin"
Plugin_Name = "CmD plugin"

two_sphere_center = [0., 0., 0.]



# ---------------------------------------INITIALIZE-----------------------------
install_dir = op.join(
    Path.home(), ".{}_installdir.txt".format(Plugin_Name_))

install_datoteka = open(install_dir, "r")
PLUGIN_DIRECTORY = install_datoteka.readline()
install_datoteka.close()
MODULE_DIRECTORY = op.join(PLUGIN_DIRECTORY, "module")
UI_DIRECTORY = op.join(PLUGIN_DIRECTORY, "UI")
#SETTINGS_DIRECTORY=op.join(PLUGIN_DIRECTORY, "settings")
PDBS_DIRECTORY = op.join(PLUGIN_DIRECTORY, "PBDs")
DOCKING_DIRECTORY = op.join(PLUGIN_DIRECTORY, "Docking")
SETTINGS_DIRECTORY = op.join(PLUGIN_DIRECTORY, "Settings")
settings = configparser.ConfigParser()

# ------------------------------------------------------------------------------


def main():

    GUI().run()


# global reference to avoid garbage collection of our dialog
dialog = None

platform = sys.platform


class GUI:
    def run(self):
        '''
        Open our custom dialog
        '''
        global dialog

        # if main is None:
        dialog = self.make_dialog()

        dialog.show()

    def make_dialog(self):
        # create a new Window
        global dialog
        dialog = QtWidgets.QMainWindow()

        # populate the Window from our *.ui file which was created with the Qt Designer
        uifile = op.join(UI_DIRECTORY, 'CmD_plugin_Main.ui')
        self.form = loadUi(uifile, dialog)

        if not op.isdir(PDBS_DIRECTORY):
            os.mkdir(PDBS_DIRECTORY)

        if not op.isdir(DOCKING_DIRECTORY):
            os.mkdir(DOCKING_DIRECTORY)

        os.chdir(PLUGIN_DIRECTORY)
        # hook up button callbacksl
        self.form.Button_Find_Bsites.clicked.connect(Receptor.Find_Bsites_clicked)
        self.form.Button_Find_Lig.clicked.connect(Receptor.Find_Lig_clicked)
        self.form.Button_Download.clicked.connect(Receptor.Download_Button_clicked)
        self.form.Button_Make_Cav_ref_lig.clicked.connect(Cavity.Make_Cavity_ref_lig)
        self.form.Button_Write_ref_lig_file.clicked.connect(Cavity.Extract_ref_lig)
        self.form.Button_Visu_Bsites.clicked.connect(Receptor.Visualise_Bsites)
        self.form.Button_Visu_Lig.clicked.connect(Receptor.Visualise_Bsites)
        self.form.Button_Write_Rec.clicked.connect(Receptor.write_Receptor_clicked)
        self.form.Button_Make_Cav_2_Sph.clicked.connect(Cavity.Make_Cavity_2_sphere)
        self.form.Button_Visu_Center.clicked.connect(Cavity.Visualise_Center)
        self.form.Button_Set_Center.clicked.connect(Cavity.Get_Center_xyz_input_line)
        self.form.Button_Open_prm.clicked.connect(Docking_func.open_prm_file)
        self.form.Button_Open_Lig.clicked.connect(Docking_func.open_lig_file)
        self.form.Button_Open_Res_sdf.clicked.connect(Docking_func.open_result_file)
        
        self.form.Button_Open_Rec.clicked.connect(Cavity.open_Rec_file)
        self.form.Button_Open_ref_lig.clicked.connect(Cavity.open_ref_lig_file)
        self.form.Button_Dock.clicked.connect(Docking_func.run_Dock)


        self.form.Button_Open_Rec_Dock_ini.clicked.connect(Run_Script_Integration.Button_Open_Rec_Dock_ini_clicked)
        #self.form.Button_Results_Output_File.clicked.connect(Run_Script_Integration.Button_Results_Output_File_clicked)
        self.form.Button_Open_Log_Output_File.clicked.connect(Run_Script_Integration.Button_Open_Log_Output_File_clicked)
        self.form.Button_Open_Docking_Dir.clicked.connect(Run_Script_Integration.Button_Open_Docking_Dir_clicked)
        self.form.Button_Open_PDBs_Dir.clicked.connect(Run_Script_Integration.Button_Open_PDBs_Dir)
        self.form.Button_Launch_Script.clicked.connect(Run_Script_Integration.Launch_Script)

        
        self.form.Button_CmDock_Loc_Open.clicked.connect(CmDock_Binaries.Open_Pressed_CmDock)
        self.form.Button_CmDock_Loc_Default.clicked.connect(CmDock_Binaries.Default_Pressed_CmDock)
               
        self.form.Button_CmCavity_Loc_Open.clicked.connect(CmDock_Binaries.Open_Pressed_CmCavity)
        self.form.Button_CmCavity_Loc_Default.clicked.connect(CmDock_Binaries.Default_Pressed_CmCavity) 
        
        self.form.Button_Display_Res.clicked.connect(CmD_Pymol_cmds.Display_Resudes_clicked)

        
        # signals
        self.form.Spin_Center_x.valueChanged.connect(Cavity.Get_Center_xyz_spin)
        self.form.Spin_Center_y.valueChanged.connect(Cavity.Get_Center_xyz_spin)
        self.form.Spin_Center_z.valueChanged.connect(Cavity.Get_Center_xyz_spin)

        self.form.Spin_Center_x.valueChanged.connect(Cavity.Update_Visu_Center)
        self.form.Spin_Center_y.valueChanged.connect(Cavity.Update_Visu_Center)
        self.form.Spin_Center_z.valueChanged.connect(Cavity.Update_Visu_Center)
        self.form.Check_Update_Visu_Center.stateChanged.connect(Cavity.Update_Visu_Center)

        self.form.Line_PDB_ID.textChanged.connect(Receptor.Find_Chains_display)
        self.form.Button_Download.clicked.connect(Receptor.Find_Chains_display)
        self.form.Line_PDB_ID.textChanged.connect(Receptor.Find_Lig_clicked)
        self.form.Button_Download.clicked.connect(Receptor.Find_Lig_clicked)
        self.form.Line_prm.textChanged.connect(Docking_func.set_suggested_out_name)
        self.form.Line_Lig.textChanged.connect(Docking_func.set_suggested_out_name)
        
        self.form.Line_PDB_ID.textChanged.connect(Smart_line_updates.update_suggested_rec_filename)
        self.form.List_Select_Chains.itemSelectionChanged.connect(Smart_line_updates.update_suggested_rec_filename)
        self.form.List_Select_Lig_Keep.itemSelectionChanged.connect(Smart_line_updates.update_suggested_rec_filename)
        self.form.Button_Download.clicked.connect(Smart_line_updates.update_suggested_rec_filename)


        self.form.List_Bsites.itemSelectionChanged.connect(Smart_line_updates.update_suggested_prm_filename_ref_lig)
        
        self.form.Spin_Center_x.valueChanged.connect(Smart_line_updates.update_suggested_prm_filename_2sp)
        self.form.Spin_Center_y.valueChanged.connect(Smart_line_updates.update_suggested_prm_filename_2sp)
        self.form.Spin_Center_z.valueChanged.connect(Smart_line_updates.update_suggested_prm_filename_2sp)
        
        self.form.Tabs_Cavity.currentChanged.connect(Smart_line_updates.tabs_cavity_changed_update_prm_fn)

        self.form.Button_Make_Cav_2_Sph.clicked.connect(Smart_line_updates.set_suggested_docking_prm_file)
        self.form.Button_Make_Cav_ref_lig.clicked.connect(Smart_line_updates.set_suggested_docking_prm_file)

        self.form.Combo_Score_Filter.currentIndexChanged.connect(docking_res.filter_docking_res)

        
        self.form.List_Docking_Score.itemSelectionChanged.connect(docking_res.update_state_docking_res)



        # Settings
        Settings.Read_Values()

        self.form.Line_CmDock_Loc.textChanged.connect(Settings.Line_CmDock_change)
        self.form.Line_CmCavity_Loc.textChanged.connect(Settings.Line_CmCavity_change)
        self.form.Check_Use_b.stateChanged.connect(Settings.Check_Use_B_change)


        self.form.Line_Log_Output_File.setText(op.join(DOCKING_DIRECTORY,"Log_Docking.txt"))
        self.form.Line_Docking_Dir.setText(DOCKING_DIRECTORY)
        self.form.Line_PDBs_Dir.setText(PDBS_DIRECTORY)

        default_python = "python"
        try:
            lib_loc = op.dirname(op.dirname(os.__file__))
            if platform == "win32":
                default_python_win = op.join(lib_loc,"python.exe")
                if op.isfile(default_python_win):
                    default_python = default_python_win
            else:
                default_python = "python3"
        except:
            pass

        self.form.Line_Python_loc.setText(default_python)


        return dialog





class Settings:
    global settings
    settings_file = op.join(SETTINGS_DIRECTORY,"Settings.ini")

    @staticmethod
    def Default_Values():
        #print("Default_Values")
        settings.add_section('settings')

        settings["settings"]["use_b_flag"] = "True"

        try:
            dir = op.join(os.environ["CMDOCK_ROOT"] ,"bin")
            fil = op.join(dir,"cmdock")
            if platform == "win32":
                fil += ".exe"
            settings["settings"]["CmDock"] = fil
            if op.isfile(fil):
                settings["settings"]["CmDock"] = fil
            else:
                settings["settings"]["CmDock"] = "cmdock"
        except:
            settings["settings"]["CmDock"] = "cmdock"
        
        try:
            dir = op.join(os.environ["CMDOCK_ROOT"] ,"bin")
            fil = op.join(dir,"cmcavity")
            if platform == "win32":
                fil += ".exe"
            if op.isfile(fil):
                settings["settings"]["CmCavity"] = fil
            else:
                settings["settings"]["CmCavity"] = "cmcavity"
                
        except:
            settings["settings"]["CmCavity"] = "cmcavity"
    


    def Read_Values():
        #print("Read_Values")

        if op.isfile(Settings.settings_file):
            try:
                settings.read(Settings.settings_file)
                Settings.Set_Values_GUI()

            except:
                print("Error with settings file, restoring default values")
                Settings.Default_Values()                
                Settings.Write_Values()
                Settings.Set_Values_GUI()
        else:
            print("No settings file found, restoring default values")

            Settings.Default_Values()                
            Settings.Write_Values()
            Settings.Set_Values_GUI()
    
    def Write_Values():
        #print("Write_Values")

        if not op.isdir(SETTINGS_DIRECTORY):
            os.makedirs(SETTINGS_DIRECTORY, exist_ok=True)

        with open(Settings.settings_file, 'w') as f:
            settings.write(f)
        
        


    def Check_Use_B_change():
        use_b = dialog.Check_Use_b.isChecked()
        settings["settings"]["use_b_flag"]  = str(use_b)
        Settings.Write_Values()
    def Line_CmDock_change():
        cmd_loc = dialog.Line_CmDock_Loc.text()
        settings["settings"]["CmDock"]  = cmd_loc
        Settings.Write_Values()
    def Line_CmCavity_change():
        cmd_loc = dialog.Line_CmCavity_Loc.text()
        settings["settings"]["CmCavity"]  = cmd_loc
        Settings.Write_Values()
    



    def Set_Values_GUI():
        #print("Set_Values_GUI")

        try:
            use_b = eval(settings["settings"]["use_b_flag"] )
            dialog.Check_Use_b.setChecked(use_b)
        except:pass

        try:
            cmd_loc = settings["settings"]["CmCavity"] 
            dialog.Line_CmCavity_Loc.setText(cmd_loc)
        except:pass

        try:
            cmd_loc = settings["settings"]["CmDock"] 
            dialog.Line_CmDock_Loc.setText(cmd_loc)
        except:pass


class CmDock_Binaries:

    @staticmethod
    def Open_Pressed_CmDock():
        msg = "Input CmDock executable location:"
        cmd_root = ""
        try:
            cmd_root = op.join(os.environ["CMDOCK_ROOT"] ,"build")
        except:
            pass
        dire = cmd_root
        #print(os.environ)
        cmd_path = Opening_GUI.Open_File(msg, dire,"")
        dialog.Line_CmDock_Loc.setText(cmd_path)


    @staticmethod
    def Default_Pressed_CmDock():
        try:
            dir = op.join(os.environ["CMDOCK_ROOT"] ,"bin")
            fil = op.join(dir,"cmdock")
            if platform == "win32":
                fil += ".exe"
            dialog.Line_CmDock_Loc.setText(fil)
        except:
            pass
        if not op.isfile(fil):
            dialog.Line_CmDock_Loc.setText("cmdock")

    @staticmethod
    def Open_Pressed_CmCavity():
        msg = "Input CmCavity executable location:"
        cmd_root = ""
        try:
            cmd_root = op.join(os.environ["CMDOCK_ROOT"] ,"bin")
        except:
            pass
        dire = cmd_root
        #print(os.environ)
        cmd_path = Opening_GUI.Open_File(msg, dire,"")
        dialog.Line_CmDock_Loc.setText(cmd_path)


    @staticmethod
    def Default_Pressed_CmCavity():
        fil = ""

        try:
            dir = op.join(os.environ["CMDOCK_ROOT"] ,"bin")
            fil = op.join(dir,"cmcavity")
            if platform == "win32":
                fil += ".exe"
            dialog.Line_CmCavity_Loc.setText(fil)
        except:
            pass
        if not op.isfile(fil):
            dialog.Line_CmCavity_Loc.setText("cmcavity")





class Opening_GUI:
    # custom file!
    @staticmethod
    def Open_File(msg, dire, format_):
        filename, _filter = QtWidgets.QFileDialog.getOpenFileName(
            dialog, msg, dire, format_)
        if filename:
            try:
                print("File found:")
                print((str(filename)))
            except:
                QtWidgets.QMessageBox.about(
                    dialog, Plugin_Name + " Warning", "File not Found! \nPlease investigate!")
        return filename
    @staticmethod
    def Open_Dir(msg, dire):
        #QtWidgets.QFileDialog.setFileMode(dialog)

        dirname = QtWidgets.QFileDialog.getExistingDirectory(
            dialog, msg, dire)
        if dirname:
            try:
                print("Directory found:")
                print((str(dirname)))
            except:
                QtWidgets.QMessageBox.about(
                    dialog, Plugin_Name + " Warning", "Directory not Found! \nPlease investigate!")
        return dirname



class subprocess_cmds:

    @staticmethod
    def mv(move_which, move_to):
        command = "mv"
        if platform == "win32":
            command = "move"
        pro = subprocess.run(
            f"{command} {move_which} {move_to}", shell=True, capture_output=True, text=True)
        # print(pro.stdout)
        # print(pro.stderr)

    @staticmethod
    def rm(rm_which):
        command = "rm"
        if platform == "win32":
            command = "del"
        if op.isfile(rm_which):
            pro = subprocess.run(f"{command} {rm_which}",
                                 shell=True, capture_output=True, text=True)
            # print(pro.stdout)
            # print(pro.stderr)


class Receptor:



    @staticmethod
    def Download_PDB_file(pdb_id):
        ftp_wwpdb_server = FTP("ftp.wwpdb.org")
        ftp_wwpdb_server.login()
        ftp_wwpdb_server.cwd(
            '/pub/pdb/data/structures/divided/pdb/' + str(pdb_id[1:3]).lower() + "/")

        ziped_fn = "pdb" + pdb_id.lower() + ".ent.gz"
        unzip_fn = pdb_id.lower() + ".pdb"
        local_file = open(ziped_fn, 'wb')
        ftp_wwpdb_server.retrbinary('RETR ' + ziped_fn, local_file.write)
        local_file.close()
        with gzip.open(ziped_fn, 'rb') as compressed_f:
            local_file_unzip = open(unzip_fn, 'wb')
            local_file_unzip.write(compressed_f.read())
            local_file_unzip.close()

        subprocess_cmds.mv(unzip_fn, PDBS_DIRECTORY)
        subprocess_cmds.rm(ziped_fn)

    @staticmethod
    def Download_Button_clicked():
        pdb_id = dialog.Line_PDB_ID.text()
        pdb_id = str(pdb_id).lower()
        if len(pdb_id) != 4:
            print("len(pdb_id) != 4")
            return

        if op.isfile(op.join(PDBS_DIRECTORY, pdb_id+".pdb")):
            print(f"{pdb_id} already present")
        else:
            print(f"Downloading {pdb_id}")
            Receptor.Download_PDB_file(pdb_id)
            print(f"Finished downloading {pdb_id}")

    @staticmethod
    def find_Chains(filename):
        chains_xyz = {}
        chain_resns = {}
        with open(filename, 'r') as fil:
            lines = fil.readlines()

            vzorec_end_model = re.compile("^ENDMDL")

            vzorec_atm = re.compile("^ATOM", re.IGNORECASE)
            for lin in lines:
                match_end = vzorec_end_model.match(lin)
                if match_end:
                    break

                match_atm = vzorec_atm.match(lin)
                if match_atm:

                    try:
                        x_B_site = float(lin[30:38])
                        y_B_site = float(lin[38:46])
                        z_B_site = float(lin[46:54])
                        chain_id = lin[21:22]
                        resn = float(lin[22:26])
                        if chain_id in chains_xyz:
                            chain_resns[chain_id].append(resn)
                            chains_xyz[chain_id].append(
                                [x_B_site, y_B_site, z_B_site])
                        else:
                            chains_xyz[chain_id] = [[x_B_site, y_B_site, z_B_site]]
                            chain_resns[chain_id] = [resn]
                    except:
                        print("err", sys.exc_info())
                        pass
        for chain in chain_resns:
            chain_resns[chain] = list(set(chain_resns[chain]))

        return chain_resns, chains_xyz

    @staticmethod
    def Find_Chains_display():
        dialog.List_Select_Chains.clear()
        pdb_id = dialog.Line_PDB_ID.text()
        pdb_id = str(pdb_id).lower()
        if len(pdb_id) != 4:
            #print("len(pdb_id) != 4",pdb_id)
            return
        pdb_file = op.join(PDBS_DIRECTORY, pdb_id + ".pdb")
        if not op.isfile(pdb_file):
            # print(pdb_file)
            return

        chain_resns, chains_xyz = Receptor.find_Chains(pdb_file)
        chain_ls = []
        for chain_id, xyz_list in chains_xyz.items():
            nr_atm = len(chain_resns[chain_id])

            avgs = [sum(map(lambda x: x[y], xyz_list))/len(xyz_list)
                    for y in [0, 1, 2]]

            chain_str = f"{chain_id}, {nr_atm} residues, avg. pos.: {avgs[0]:5.2f}, {avgs[1]:5.2f}, {avgs[2]:5.2f}"
            chain_ls.append([chain_str, nr_atm])
        print(chain_ls)
        for chain_str, nr_atm in sorted(chain_ls, key=lambda x: x[1], reverse=True):
            dialog.List_Select_Chains.addItem(chain_str)
            print(chain_str)

    @staticmethod
    def find_B_sites(filename):
        b_sites = {}

        with open(filename, 'r') as fil:
            lines = fil.readlines()

            vzorec_end_model = re.compile("^ENDMDL")

            vzorec_hetatm = re.compile("^HETATM", re.IGNORECASE)
            for lin in lines:
                match_end = vzorec_end_model.match(lin)
                if match_end:
                    break

                match_hetatm = vzorec_hetatm.match(lin)
                if match_hetatm:
                    if lin[17:20].strip() == 'HOH':
                        pass

                    # print(lin.strip())

                    x_B_site = float(lin[30:38])
                    y_B_site = float(lin[38:46])
                    z_B_site = float(lin[46:54])
                    bsite_id = lin[17:20].strip()+'_' + \
                        lin[22:26].strip()+'_'+lin[21:22].strip()
                    if bsite_id in b_sites:
                        b_sites[bsite_id].append([x_B_site, y_B_site, z_B_site])
                    else:
                        b_sites[bsite_id] = [[x_B_site, y_B_site, z_B_site]]

        return b_sites

    @staticmethod
    def Find_Bsites_clicked():
        pdb_id = dialog.Line_PDB_ID.text()
        pdb_id = str(pdb_id).lower()
        if len(pdb_id) != 4:
            print("len(pdb_id) != 4")
            return
        water_bsites = dialog.Check_Water_Bsite.isChecked()
        bsites = Receptor.find_B_sites(op.join(PDBS_DIRECTORY, pdb_id + ".pdb"))
        dialog.List_Bsites.clear()
        bsite_list = []
        for bsite_id, xyz_list in bsites.items():
            nr_atm = len(xyz_list)

            if nr_atm > 0:
                if not water_bsites:
                    if bsite_id.split("_")[0] == "HOH":
                        continue
                avgs = [sum(map(lambda x: x[y], xyz_list)) /
                        nr_atm for y in [0, 1, 2]]

                bsite_str = f"{bsite_id}, {nr_atm} atoms,  average pos.:  {avgs[0]:5.2f}, {avgs[1]:5.2f}, {avgs[2]:5.2f}"
                bsite_list.append([bsite_str, nr_atm])

        for bsite_str, nr_atm in sorted(bsite_list, key=lambda x: x[1], reverse=True):
            dialog.List_Bsites.addItem(bsite_str)
            print(bsite_str)
    

    @staticmethod
    def Find_Lig_clicked():
        dialog.List_Select_Lig_Keep.clear()

        pdb_id = dialog.Line_PDB_ID.text()
        pdb_id = str(pdb_id).lower()
        if len(pdb_id) != 4:
            return
        pdb_file = op.join(PDBS_DIRECTORY, pdb_id + ".pdb")
        if not op.isfile(pdb_file):
            # print(pdb_file)
            return
        water_bsites = dialog.Check_Water_Lig.isChecked()
        bsites = Receptor.find_B_sites(op.join(PDBS_DIRECTORY, pdb_id + ".pdb"))
        dialog.List_Select_Lig_Keep.clear()
        bsite_list = []
        for bsite_id, xyz_list in bsites.items():
            nr_atm = len(xyz_list)

            if nr_atm > 0:
                if not water_bsites:
                    if bsite_id.split("_")[0] == "HOH":
                        continue
                avgs = [sum(map(lambda x: x[y], xyz_list)) /
                        nr_atm for y in [0, 1, 2]]

                bsite_str = f"{bsite_id}, {nr_atm} atoms,  average pos.:  {avgs[0]:5.2f}, {avgs[1]:5.2f}, {avgs[2]:5.2f}"
                bsite_list.append([bsite_str, nr_atm])

        for bsite_str, nr_atm in sorted(bsite_list, key=lambda x: x[1], reverse=True):
            dialog.List_Select_Lig_Keep.addItem(bsite_str)
            print(bsite_str)



    @staticmethod
    def Visualise_Bsites():
        Receptor.Find_Bsites_clicked()
        Receptor.Find_Lig_clicked()

        pdb_id = dialog.Line_PDB_ID.text()
        pdb_id = str(pdb_id).lower()
        if len(pdb_id) != 4:
            print("len(pdb_id) != 4")
            return
        CmD_Pymol_cmds.Rein_load_rec(pdb_id)

        bsites = Receptor.find_B_sites(op.join(PDBS_DIRECTORY, pdb_id + ".pdb"))
        water_bsites = dialog.Check_Water_Bsite.isChecked()
        for bsite_id, xyz_list in bsites.items():
            nr_atm = len(xyz_list)
            if not water_bsites:
                if bsite_id.split("_")[0] == "HOH":
                    continue
            avgs = [sum(map(lambda x: x[y], xyz_list))/nr_atm for y in [0, 1, 2]]
            #bsite_lig,bsite_resi,bsite_chain = bsite_id.split("_")
            cmd.do(f"pseudoatom {bsite_id}, pos=[{avgs[0]},{avgs[1]},{avgs[2]}]")

            cmd.do(f"label {bsite_id}, '{bsite_id}'")
        cmd.do("set label_size, -2")
    
    @staticmethod
    def get_selected_chain_ls():
    
        chain_ls = []
        if dialog.List_Select_Chains.selectedItems():
            for item in dialog.List_Select_Chains.selectedItems():
                chain = item.text().split(",")[0]
                chain_ls.append(chain)
        if not chain_ls: # no chains selected, select all chains
            for x in range(dialog.List_Select_Chains.count()):
                item = dialog.List_Select_Chains.item(x)
                chain = item.text().split(",")[0]
                
                chain_ls.append(chain)

        return chain_ls
    


    @staticmethod
    def get_selected_ligand_keep_ls():
    
        lig_ls = []
        if dialog.List_Select_Lig_Keep.selectedItems():
            for item in dialog.List_Select_Lig_Keep.selectedItems():
                lig = item.text().split(",")[0]
                lig_ls.append(lig)

        return lig_ls
    
    
    @staticmethod
    def write_Receptor(pdb_id,chain_ls,lig_ls,rec_save_fn):
        CmD_Pymol_cmds.Rein_load_rec(pdb_id)

        
        for i, chain in enumerate(chain_ls):

            cmd.do(f"select to_keep, polymer and chain {chain},merge=1")

        for i, lig in enumerate(lig_ls):
            lig_name, lig_resi, lig_chain = lig.split("_")
            cmd.do(f"select to_keep, resi {lig_resi} and chain {lig_chain},merge=1")

        

        rm_cmd = f"remove not to_keep"
        cmd.do(rm_cmd)
        cmd.deselect()        
        cmd.save(op.join(DOCKING_DIRECTORY, rec_save_fn), pdb_id)
    

    def write_Receptor_clicked():
        pdb_id = dialog.Line_PDB_ID.text()
        pdb_id = str(pdb_id).lower()
        if len(pdb_id) != 4:
            print("len(pdb_id) != 4")
            return
        chain_ls = Receptor.get_selected_chain_ls()
        lig_ls = Receptor.get_selected_ligand_keep_ls()
        rec_save_fn = dialog.Line_Rec_Out_Name.text()
        Receptor.write_Receptor(pdb_id,chain_ls,lig_ls,rec_save_fn)
        
        dialog.Line_Rec_Cavity.setText(rec_save_fn)
        dialog.Tabs_main.setCurrentIndex(1)
        Receptor.Find_Bsites_clicked()





class CmD_Pymol_cmds:

    @staticmethod
    def Extract_Lig(pdb_id, ligand_resi, ligand_chain, lig_save_fn,lig_name):
        CmD_Pymol_cmds.Rein_load_rec(pdb_id)

        cmd.do(f"select lig, resi {ligand_resi} and chain {ligand_chain}")
        cmd.do(f"extract {lig_name},  lig")
        cmd.save(op.join(DOCKING_DIRECTORY, lig_save_fn), lig_name)


    @staticmethod
    def Make_Prm(rec_save_fn, lig_save_fn, prm_fn, title, template_file):

        with open(template_file, "r") as prm_template:
            template_lines = prm_template.readlines()

        rep_title = "REPLACE_prm_title"
        rep_rec_fn = "REPLACE_rec_filename"
        rep_ref_mol_fn = "REPLACE_ref_mol_filename"
        rep_rad = "REPLACE_radius"
        rep_small_sp = "REPLACE_small_sphere"

        rep_min_vol = "REPLACE_min_vol"
        rep_max_cav = "REPLACE_max_cav"
        rep_vol_incr = "REPLACE_vol_incr"
        rep_gridstep = "REPLACE_gridstep"

        '''
        REF_MOL REPLACE_ref_mol_filename
        RADIUS REPLACE_radius
        SMALL_SPHERE REPLACE_small_sphere
        MIN_VOLUME REPLACE_min_vol
        MAX_CAVITIES REPLACE_max_cav
        VOL_INCR REPLACE_vol_incr
        GRIDSTEP REPLACE_gridstep 
        '''

        rep_center = "REPLACE_center"
        rep_large_sp = "REPLACE_large_sphere"
        '''
        LARGE_SPHERE REPLACE_large_sphere
        CENTER REPLACE_center
        '''
        out_lines = []
        for ln in template_lines:
            if "REPLACE_" in ln:
                if rep_title in ln:
                    out_lines.append(ln.replace(rep_title, title))
                elif rep_rec_fn in ln:
                    out_lines.append(ln.replace(rep_rec_fn, rec_save_fn))
                elif rep_ref_mol_fn in ln:
                    out_lines.append(ln.replace(rep_ref_mol_fn, lig_save_fn))

                elif rep_rad in ln:
                    out_lines.append(ln.replace(
                        rep_rad, str(dialog.Spin_Radius.value())))
                elif rep_small_sp in ln:
                    out_lines.append(ln.replace(rep_small_sp, str(
                        dialog.Spin_Small_Sphere.value())))
                elif rep_min_vol in ln:
                    out_lines.append(ln.replace(
                        rep_min_vol, str(dialog.Spin_Min_Vol.value())))
                elif rep_max_cav in ln:
                    out_lines.append(ln.replace(
                        rep_max_cav, str(dialog.Spin_Max_Cav.value())))
                elif rep_vol_incr in ln:
                    out_lines.append(ln.replace(
                        rep_vol_incr, str(dialog.Spin_Vol_Incr.value())))
                elif rep_gridstep in ln:
                    out_lines.append(ln.replace(
                        rep_gridstep, str(dialog.Spin_Gridstep.value())))
                elif rep_center in ln:
                    global two_sphere_center
                    center_str = f"({two_sphere_center[0]},{two_sphere_center[1]},{two_sphere_center[2]})"
                    out_lines.append(ln.replace(rep_center, center_str))
                elif rep_large_sp in ln:
                    out_lines.append(ln.replace(rep_large_sp, str(
                        dialog.Spin_Large_Sphere.value())))

            else:
                out_lines.append(ln)

        with open(op.join(DOCKING_DIRECTORY, prm_fn), "w") as prm:
            for ln in out_lines:
                prm.write(ln)

    @staticmethod
    def Run_cmcavity(exe_call,prm_fn):
        os.chdir(DOCKING_DIRECTORY)
        grd_file = f"{prm_fn[:-4]}_cav1.mrc"

        cav_file = f"{prm_fn[:-4]}.as"

        subprocess_cmds.rm(cav_file)
        subprocess_cmds.rm(grd_file)
        cav_cmd = f"{exe_call} -r {prm_fn} -W -M"
        pro = subprocess.run(cav_cmd, capture_output=True,text=True, shell=True)
        
        print(cav_cmd)
        print(pro.stdout)
        print(pro.stderr)
        #print("return code: ",pro.returncode)

        os.chdir(PLUGIN_DIRECTORY)
        return pro.returncode


    @staticmethod
    def Visu_cavity_grd(grd_file,receptor_file,ligand_file = ""):
        cmd.reinitialize()
        os.chdir(DOCKING_DIRECTORY)
        cmd.do(f"load {op.join(DOCKING_DIRECTORY,grd_file)}")
        cmd.do(f"isomesh cav1, {grd_file[:-4]}, 0.99")
        cmd.do(f"load {receptor_file}")
        if ligand_file:
            cmd.do(f"load {op.join(DOCKING_DIRECTORY,ligand_file)}")

        os.chdir(PLUGIN_DIRECTORY)


    @staticmethod
    def Rein_load_rec(pdb_id):
        cmd.do(f"rein")
        rec_file = op.join(PDBS_DIRECTORY, pdb_id+".pdb")
        cmd.do(f"load {rec_file}")

    @staticmethod
    def dock_cmdock(prm_file, lig_file, out_file, n_poses, n_kept_poses,use_b,exe_call):
        if use_b:
            b_str = f" -b {n_kept_poses} "
        else:
            b_str = " "
        
        
        

        dock_cmd = f"{exe_call} -r {prm_file} -p dock.prm -i {lig_file} -n {n_poses}{b_str}-o {out_file}"
        pro = subprocess.run(dock_cmd,
            capture_output=True,text=True, shell=True)
        
        print(dock_cmd)
        print(pro.stdout)
        print(pro.stderr)
        #print("return code: ",pro.returncode)

        return pro.returncode

    

    @staticmethod
    def display_residues(around):
        # display residues around
        cmd.select("close_residues", f"{around} around 4")
        cmd.select("byres close_residues")
        cmd.show("sticks", "byres close_residues")
        cmd.util.cbay("byres close_residues")
        cmd.set_bond("stick_radius", "0.1", "byres close_residues")
        cmd.select("sele", "name ca and byres close_residues")
        cmd.show("sticks", "organic")
        cmd.color("blue", "organic")
        cmd.util.cnc ("organic")
        cmd.set_bond("stick_radius", "0.25", "organic")
    
    
    @staticmethod
    def Display_Resudes_clicked():

        out_file = dialog.Line_Docking_Out.text()
        out_name = out_file[:-4]
        print(out_file,out_name)
        CmD_Pymol_cmds.display_residues(out_name)



no_write_two_sphere_line = False


class Cavity:
        

    @staticmethod
    def Get_Center_xyz_input_line():
        global two_sphere_center

        center_inp = dialog.Line_Center_inp.text()

        center_inp_spl1 = center_inp.strip().split()
        center_inp_spl2 = []
        for sp in center_inp_spl1:
            for s in sp.split(","):
                center_inp_spl2.append(s)

        while "" in center_inp_spl2:
            center_inp_spl2.remove("")
        # print(center_inp_spl2)

        if len(center_inp_spl2) != 3:
            return
        try:
            center = [float(x) for x in center_inp_spl2]
        except:
            return
        old_check = dialog.Check_Update_Visu_Center.isChecked()
        if old_check:
            dialog.Check_Update_Visu_Center.setChecked(False)
        global no_write_two_sphere_line
        no_write_two_sphere_line = True
        dialog.Spin_Center_x.setValue(center[0])
        dialog.Spin_Center_y.setValue(center[1])
        dialog.Spin_Center_z.setValue(center[2])
        no_write_two_sphere_line = False
        if old_check:
            dialog.Check_Update_Visu_Center.setChecked(True)
        # Get_Center_xyz_spin()

    @staticmethod
    def Get_Center_xyz_spin():
        global two_sphere_center

        two_sphere_center = (dialog.Spin_Center_x.value(
        ), dialog.Spin_Center_y.value(), dialog.Spin_Center_z.value())
        global no_write_two_sphere_line
        if not no_write_two_sphere_line:
            line_str = f"{two_sphere_center[0]},  {two_sphere_center[1]},  {two_sphere_center[2]}"
            dialog.Line_Center_inp.setText(line_str)
            # print("Get_Center_xyz_spin")
            # print(line_str)

    @staticmethod
    def Visualise_Center():
        if dialog.Line_Rec_Cavity.text():
            cmd.do("rein")
            cmd.load(dialog.Line_Rec_Cavity.text())
        else:
            pdb_id = dialog.Line_PDB_ID.text()
            pdb_id = str(pdb_id).lower()
            if len(pdb_id) != 4:
                print("len(pdb_id) != 4")
                return
            CmD_Pymol_cmds.Rein_load_rec(pdb_id)

        cmd.do("set auto_zoom, off ")

        cmd.do(f"remove center_2sp")
        cmd.do("delete axes")
        Cavity.Draw_Center()
        Cavity.Draw_Axes_to_Center()
        #cmd.do(f"translate {center_str},object=axes")

    @staticmethod
    def Update_Visu_Center():
        if dialog.Check_Update_Visu_Center.isChecked():
            cmd.do(f"remove center_2sp")
            cmd.do("delete axes")
            Cavity.Draw_Center()

            #rot_mat = [0 for i in range(16)]
            #rot_mat[3] = dialog.Spin_Center_x.value()
            #rot_mat[7] = dialog.Spin_Center_y.value()
            #rot_mat[11] = dialog.Spin_Center_z.value()
            # print(rot_mat)

            #cmd.do(f"translate {center_str},object=axes")
            # cmd.set_object_ttt("axes",rot_mat)

    @staticmethod
    def Draw_Center():
        global two_sphere_center
        center_str = f"[{two_sphere_center[0]},{two_sphere_center[1]},{two_sphere_center[2]}]"
        cmd.do(f"pseudoatom center_2sp, pos={center_str},color=red,vdw=0.5")
        cmd.do("as spheres, center_2sp")
        Cavity.Draw_Axes_to_Center()

    @staticmethod
    def Draw_Axes_to_Center():
        w = 0.06  # cylinder width
        l = 1  # cylinder length
        h = 0.25  # cone hight
        d = w * 1.618  # cone base diameter
        global two_sphere_center

        x = two_sphere_center[0]
        y = two_sphere_center[1]
        z = two_sphere_center[2]
        obj = [CYLINDER, x, y, z,   x+l, y, z, w, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0,
            CYLINDER, x, y, z,   x, y+l, z, w, 0.0, 1.0, 0.0, 0.0, 1.0, 0.0,
            CYLINDER, x, y, z,   x, y, z+l, w, 0.0, 0.0, 1.0, 0.0, 0.0, 1.0,
            CONE,   x+l, y, z, x+h+l, y, z, d, 0.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 1.0,
            CONE, x, y+l, z, x, y+h+l, z, d, 0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 0.0, 1.0, 1.0,
            CONE, x, y, z+l, x, y, z+h+l, d, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 1.0, 1.0]
        cyl_text(obj, plain, [x+l*1.25, y, z], 'X', 0.04,
                axes=[[0.4, 0, 0], [0, 0.4, 0], [0, 0, 0.4]])
        cyl_text(obj, plain, [x, y+l*1.25, z], 'Y', 0.04,
                axes=[[0.4, 0, 0], [0, 0.4, 0], [0, 0, 0.4]])
        cyl_text(obj, plain, [x, y, z+l*1.25], 'Z', 0.04,
                axes=[[0.4, 0, 0], [0, 0.4, 0], [0, 0, 0.4]])

        '''obj = [CYLINDER, 0.0, 0.0, 0.0,   l, 0.0, 0.0, w, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0,
            CYLINDER, 0.0, 0.0, 0.0, 0.0,   l, 0.0, w, 0.0, 1.0, 0.0, 0.0, 1.0, 0.0,
            CYLINDER, 0.0, 0.0, 0.0, 0.0, 0.0,   l, w, 0.0, 0.0, 1.0, 0.0, 0.0, 1.0,
            CONE,   l, 0.0, 0.0, h+l, 0.0, 0.0, d, 0.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 1.0, 
            CONE, 0.0,   l, 0.0, 0.0, h+l, 0.0, d, 0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 0.0, 1.0, 1.0, 
            CONE, 0.0, 0.0,   l, 0.0, 0.0, h+l, d, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 1.0, 1.0]
        cyl_text(obj,plain,[l*1.25,0.,0.],'X',0.05,axes=[[0.5,0,0],[0,0.5,0],[0,0,0.5]])
        cyl_text(obj,plain,[0.,l*1.25,0.],'Y',0.05,axes=[[0.5,0,0],[0,0.5,0],[0,0,0.5]])
        cyl_text(obj,plain,[0.,0.,l*1.25],'Z',0.05,axes=[[0.5,0,0],[0,0.5,0],[0,0,0.5]])'''

        cmd.load_cgo(obj, 'axes')
        #print("and my ax")
    
    @staticmethod
    def open_Rec_file():
        msg = "Input Receptor File:"
        dire = DOCKING_DIRECTORY
        format_ = "mol2 File (*.mol2)"
        fn = Opening_GUI.Open_File(msg, dire, format_)

        dialog.Line_Rec_Cavity.setText(fn)
    
    @staticmethod
    def open_ref_lig_file():
        msg = "Input reference ligand File:"
        dire = DOCKING_DIRECTORY
        format_ = "sdf File (*.sdf)"
        fn = Opening_GUI.Open_File(msg, dire, format_)

        dialog.Line_ref_lig_file.setText(fn)
    @staticmethod
    def Extract_ref_lig():
        pdb_id = dialog.Line_PDB_ID.text()
        pdb_id = str(pdb_id).lower()
        if len(pdb_id) != 4:
            print("len(pdb_id) != 4")
            return
        bsite = str(dialog.List_Bsites.currentItem().text()).split(",")[0]
        bsite_lig, bsite_resi, bsite_chain = bsite.split("_")

        ref_lig_save_fn = f"ligand_{bsite}_{pdb_id}.sdf"
        lig_name = f"{bsite_lig}"
        CmD_Pymol_cmds.Extract_Lig(pdb_id,bsite_resi,bsite_chain,ref_lig_save_fn,lig_name)
        dialog.Line_ref_lig_file.setText(op.join(DOCKING_DIRECTORY,ref_lig_save_fn))
    
    @staticmethod
    def Make_Cavity_ref_lig():
        #pdb_id = dialog.Line_PDB_ID.text()
        #pdb_id = str(pdb_id).lower()
        #if len(pdb_id) != 4:
        #    print("len(pdb_id) != 4")
        #    return
        #bsite = str(dialog.List_Bsites.currentItem().text()).split(",")[0]
        #bsite_lig, bsite_resi, bsite_chain = bsite.split("_")

        prm_fn = dialog.Line_prm_Out_Name.text()  
        rec_save_fn = dialog.Line_Rec_Cavity.text()#dialog.Line_Rec_Out_Name.text()   #f"receptor_{pdb_id}.mol2"
        
        #ref_lig_save_fn = f"ligand_{bsite}_{pdb_id}.sdf"
        
            
        #lig_name = f"{bsite_lig}"
        #CmD_Pymol_cmds.Extract_Lig(pdb_id,bsite_resi,bsite_chain,ref_lig_save_fn,lig_name)
        #chain_ls = Receptor.get_selected_chain_ls()
        #if not chain_ls:
        #    chain_ls = [bsite_chain]
        ref_lig_save_fn = dialog.Line_ref_lig_file.text()
        

        template_file = op.join(PLUGIN_DIRECTORY, "ref_lig_prm_template.prm")
        CmD_Pymol_cmds.Make_Prm(rec_save_fn, ref_lig_save_fn, prm_fn, f"{Path(rec_save_fn).stem}_{Path(ref_lig_save_fn).stem}", template_file)

        cmcavity_exe = dialog.Line_CmCavity_Loc.text()
        ret_code = CmD_Pymol_cmds.Run_cmcavity(cmcavity_exe,prm_fn)
        if ret_code:
            QtWidgets.QMessageBox.about(
                    dialog, Plugin_Name + " Warning", "An error occured with CmCavity \nPlease investigate!")
            return
        grd_file = f"{prm_fn[:-4]}_cav1.mrc"
        CmD_Pymol_cmds.Visu_cavity_grd(grd_file,rec_save_fn,ref_lig_save_fn)

        print()
        print()
        if op.isfile(op.join(DOCKING_DIRECTORY,prm_fn)) and op.isfile(op.join(DOCKING_DIRECTORY,prm_fn[:-4]+".as")):
            print(f"Written .prm file {prm_fn}")
            print(f"Used receptor file {rec_save_fn}")
            print(f"Used reference ligand file {ref_lig_save_fn}")

    @staticmethod
    def Make_Cavity_2_sphere():


        prm_fn = dialog.Line_prm_Out_Name.text()  
        rec_save_fn = dialog.Line_Rec_Cavity.text()#dialog.Line_Rec_Out_Name.text() 


        chain_ls = Receptor.get_selected_chain_ls()


        template_file = op.join(PLUGIN_DIRECTORY, "2_sphere_prm_template.prm")
        CmD_Pymol_cmds.Make_Prm(rec_save_fn, "", prm_fn,
                                f"{Path(rec_save_fn).stem}_two_spheres", template_file)


        cmcavity_exe = dialog.Line_CmCavity_Loc.text()
        ret_code = CmD_Pymol_cmds.Run_cmcavity(cmcavity_exe,prm_fn)
        if ret_code:
            QtWidgets.QMessageBox.about(
                    dialog, Plugin_Name + " Warning", "An error occured with CmCavity \nPlease investigate!")
            return
        grd_file = f"{prm_fn[:-4]}_cav1.mrc"

        CmD_Pymol_cmds.Visu_cavity_grd(grd_file,rec_save_fn)
        Cavity.Update_Visu_Center()
        if op.isfile(op.join(DOCKING_DIRECTORY,prm_fn)) and op.isfile(op.join(DOCKING_DIRECTORY,prm_fn[:-4]+".as")):
            print(f"Written .prm file {rec_save_fn}")
            print(f"Used receptor file {rec_save_fn}")














class Smart_line_updates:

    @staticmethod
    def update_suggested_rec_filename():
        chain_ls = Receptor.get_selected_chain_ls()
        lig_ls = Receptor.get_selected_ligand_keep_ls()

        pdb_id = dialog.Line_PDB_ID.text()
        pdb_id = str(pdb_id).lower()
        if len(pdb_id) != 4:
            return
        rec_fn_start = f"rec_{pdb_id}_"
        if not chain_ls:
            rec_fn_start = f"rec_{pdb_id}"
        
        for ch in chain_ls:
            rec_fn_start += ch
        
        for lig in lig_ls:
            rec_fn_start += "_" +  lig
        rec_fn = rec_fn_start +".mol2"
        dialog.Line_Rec_Out_Name.setText(rec_fn)


    @staticmethod
    def tabs_cavity_changed_update_prm_fn():
        ind = dialog.Tabs_Cavity.currentIndex() 
        
        if ind == 0: # ref lig
            Smart_line_updates.update_suggested_prm_filename_ref_lig()
        elif ind == 1: # 2 sphere
            Smart_line_updates.update_suggested_prm_filename_2sp()
    @staticmethod
    def update_suggested_prm_filename_ref_lig():
        try:
            pdb_id = dialog.Line_PDB_ID.text()
            pdb_id = str(pdb_id).lower()
            if len(pdb_id) != 4:
                return
            bsite = str(dialog.List_Bsites.currentItem().text()).split(",")[0]
            bsite_lig, bsite_resi, bsite_chain = bsite.split("_")

            
            prm_fn = f"{pdb_id}_{bsite}.prm"

            dialog.Line_prm_Out_Name.setText(prm_fn)
        except:
            pass
    @staticmethod
    def update_suggested_prm_filename_2sp():
        try:
            pdb_id = dialog.Line_PDB_ID.text()
            pdb_id = str(pdb_id).lower()
            if len(pdb_id) != 4:
                return

            
            prm_fn = f"{pdb_id}_2sp.prm"

            dialog.Line_prm_Out_Name.setText(prm_fn)
        except:
            pass
    
    
    @staticmethod
    def set_suggested_docking_prm_file():
        current_prm = dialog.Line_prm_Out_Name.text()
        if current_prm:
            dialog.Line_prm.setText(op.join(DOCKING_DIRECTORY,current_prm))


















class Docking_func:
    @staticmethod
    def open_prm_file():
        msg = "Input .prm File:"
        dire = DOCKING_DIRECTORY
        format_ = "prm File (*.prm)"
        fn = Opening_GUI.Open_File(msg, dire, format_)

        dialog.Line_prm.setText(fn)

    @staticmethod
    def open_lig_file():
        msg = "Input Ligand File:"
        dire = DOCKING_DIRECTORY
        format_ = "sdf File (*.sdf)"
        fn = Opening_GUI.Open_File(msg, dire, format_)

        dialog.Line_Lig.setText(fn)


    @staticmethod
    def set_suggested_out_name():
        prm_file = dialog.Line_prm.text()
        lig_file = dialog.Line_Lig.text()

        if not (prm_file and lig_file):
            return
        
        prm_file_ne = Path(prm_file).stem
        lig_file_ne = Path(lig_file).stem
        suggested_out_name = f"Dock_{prm_file_ne}_{lig_file_ne}.sdf"

        dialog.Line_Docking_Out.setText(suggested_out_name)

    @staticmethod
    def open_result_file():
        msg = "Input CmDock Result File:"
        dire = DOCKING_DIRECTORY
        format_ = "sdf File (*.sdf)"
        fn = Opening_GUI.Open_File(msg, dire, format_)

        #dialog.Line_Lig.setText(fn)
        Docking_func.visu_result_file(fn)


    @staticmethod
    def visu_result_file(out_file,prm_loc_file = None):
        cmd.do("rein")
        cmd.do(f"load {out_file}")
        docking_res.parse_sdf(out_file)
        docking_res.filter_docking_res()
        if prm_loc_file:
            #try:
            with open(prm_loc_file,"r") as prm_f:
                for line in prm_f.readlines():
                    if "RECEPTOR_FILE" in line:
                        rec_fl_spl = line.strip().split()
                        while "" in rec_fl_spl:
                            rec_fl_spl.remove("")
                        rec_file = rec_fl_spl[1]
                        cmd.do(f"load {rec_file}") # load receptor
                        
                        break
                
            #grd_file = f"{prm_loc_file[:-4]}_cav1.grd"
            grd_file = f"{prm_loc_file[:-4]}_cav1.mrc"

            cmd.do(f"load {grd_file}")
            cmd.do(f"isomesh cav1, {grd_file[:-4]}, 0.99")


    @staticmethod
    def run_Dock():
        prm_file = dialog.Line_prm.text()
        lig_file = dialog.Line_Lig.text()

        out_file = dialog.Line_Docking_Out.text()

        n_poses = dialog.Spin_N_Poses.value()
        n_kept_poses = dialog.Spin_N_Kept_Poses.value()
        use_b = dialog.Check_Use_b.isChecked()

        prm_dir = op.dirname(prm_file)
        prm_loc_file = op.basename(prm_file)
        cmdock_exe = dialog.Line_CmDock_Loc.text()

        os.chdir(prm_dir)
        ret_code = CmD_Pymol_cmds.dock_cmdock(prm_loc_file, lig_file, out_file, n_poses, n_kept_poses,use_b,cmdock_exe)
        
        if ret_code:
            QtWidgets.QMessageBox.about(
                    dialog, Plugin_Name + " Warning", "An error occured with CmDock \nPlease investigate!")
            return
        if not op.isfile(out_file):        
            return

        Docking_func.visu_result_file(out_file,prm_loc_file)
        
        os.chdir(PLUGIN_DIRECTORY)


ligand_entries = []



class docking_res:
    @staticmethod
    def parse_sdf(sdf_file):
        global ligand_entries
        with open(sdf_file,"r") as sdf:
            lines = [ i.strip() for i  in sdf.readlines()]
        
        c = 0
        #end_m = False
        name = ""
        #end_m_str = "M  END"
        entry_str = ">  <"
        end_str = "$$$$"
        entry_dict = {}

        ligand_entries = []

        for ln_i,line in enumerate(lines):
            if c == 0:
                name = line
            c += 1
            if line == end_str:
                
                ligand_entries.append([name,entry_dict])
                name = ""
                c = 0
                entry_dict = {}
                continue
            
            if entry_str in line:
                entry = line.replace(entry_str,"")
                entry = entry.replace(">","")
                
                entry_dict[entry] = lines[ln_i+1]
        

        docking_res.make_Combo_Score_Filter()
    
    def make_Combo_Score_Filter():
        count = dialog.Combo_Score_Filter.count()
        for i in range(count):
            dialog.Combo_Score_Filter.removeItem(0)
        
        if ligand_entries:
            for entry in ligand_entries[0][1]:
                if "SCORE" in entry:
                    dialog.Combo_Score_Filter.addItem(entry)
    
    
    def filter_docking_res():
        dialog.List_Docking_Score.clear()

        filter_ = dialog.Combo_Score_Filter.currentText()
        ls = []
        #name_dic = {}
        c = 0
        for name, entry_dic in ligand_entries:
            #if name not in name_dic:
            #    name_dic[name] = 1
            #else:
            #    name_dic[name] += 1
            c += 1
            #ls.append([name, name_dic[name],entry_dic[filter_]])
            ls.append([name, c ,entry_dic[filter_]])
        

        ls = sorted(ls,key=lambda x: float(x[2]))
        
        cmd.do(f"set state, {ls[0][1]}")
        for name, name_nr, f_score in ls:
            out_str = f"{name} {name_nr: <8} {filter_}:   {f_score}"
            dialog.List_Docking_Score.addItem(out_str)
    

    def update_state_docking_res():
        selected_item = dialog.List_Docking_Score.currentItem().text()

        if selected_item:
            try:
                item_spl = selected_item.split()
                while "" in item_spl:
                    item_spl.remove("")
                item_state = int(item_spl[1])
                #print(item_spl)

                cmd.do(f"set state, {item_state}")

            except:
                pass





class Run_Script_Integration:
    
    @staticmethod
    def Button_Open_Rec_Dock_ini_clicked():
        msg = "Input Autmoatic Run Input File:"
        dire = DOCKING_DIRECTORY
        format_ = "ini File (*.ini)"
        fn = Opening_GUI.Open_File(msg, dire, format_)
        dialog.Line_Rec_Dock_ini.setText(fn)
    
    '''@staticmethod
    def Button_Results_Output_File_clicked():
        msg = "Input Filename to write results in:"
        dire = DOCKING_DIRECTORY
        format_ = "text File (*)"
        fn = Opening_GUI.Open_File(msg, dire, format_)
        dialog.Line_Results_Output_File.setText(fn)'''
   
    @staticmethod
    def Button_Open_Log_Output_File_clicked():
        msg = "Input Filename to write log in:"
        dire = DOCKING_DIRECTORY
        format_ = "text File (*)"
        fn = Opening_GUI.Open_File(msg, dire, format_)
        dialog.Line_Log_Output_File.setText(fn)


    @staticmethod
    def Button_Open_Docking_Dir_clicked():
        msg = "Input Directory to perform docking in:"
        dire = DOCKING_DIRECTORY
        fn = Opening_GUI.Open_Dir(msg, dire)
        dialog.Line_Docking_Dir.setText(fn)

    @staticmethod
    def Button_Open_PDBs_Dir():
        msg = "Input Directory to perform docking in:"
        dire = PDBS_DIRECTORY
        fn = Opening_GUI.Open_Dir(msg, dire)
        dialog.Line_PDBs_Dir.setText(fn)
    


    @staticmethod
    def Launch_Script():
        ini_file = dialog.Line_Rec_Dock_ini.text()
        log_file = dialog.Line_Log_Output_File.text()
        Dock_Dir = dialog.Line_Docking_Dir.text()
        PDBs_dir = dialog.Line_PDBs_Dir.text()
        
        python_in = dialog.Line_Python_loc.text()
        n_proc = dialog.Spin_N_Multiproc.value()

        script_fn = op.join(MODULE_DIRECTORY,"Run_CmDock_Input.py")
        
        cmd = f"{python_in} {script_fn} -i {ini_file} -d {Dock_Dir} -pd {PDBs_dir} -n {n_proc} -l {log_file} &"

        #cmd_ls = [python_in, script_fn, "-i", ini_file, "-d", Dock_Dir, "-pd", PDBs_dir, "-n", n_proc, "-l", log_file, "&"]
        #cmd_ls = [str(i) for i in cmd_ls]
        print(cmd)
        
        ini_dir = op.dirname(ini_file)
        pro = subprocess.Popen(cmd,shell=True,cwd=ini_dir,stdout=subprocess.PIPE,stderr=subprocess.STDOUT,universal_newlines=True)
        #stdout,stderr = pro.communicate()
        #print(stdout)
        QtWidgets.QMessageBox.about(dialog, Plugin_Name + " Message", f"Launched Run_CmDock_Input.py form input file: \n{ini_file}")


