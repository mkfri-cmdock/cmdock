#include "RbtHeap.hpp"

RbtHeap::Ligand::Ligand(double dScore, std::vector<std::string> vLigand) {
    score = dScore;
    ligand = vLigand;
}

RbtHeap::Heap::Heap(unsigned long uSize) {
    limit = uSize;
}

void RbtHeap::Heap::setSize(unsigned long uSize) {
    limit = uSize;
}

size_t RbtHeap::Heap::size() {
    return heap.size();
}

RbtHeap::Ligand RbtHeap::Heap::top() {
    return heap.top();
}

void RbtHeap::Heap::pop() {
    heap.pop();
}

bool RbtHeap::Heap::empty() {
    return heap.empty();
}

void RbtHeap::Heap::push(Ligand newLigand) {
    heap.push(newLigand);
    while (heap.size() > limit) {
        heap.pop();
    }
}

void RbtHeap::Heap::clear() {
    heap = std::priority_queue<Ligand, std::vector<Ligand>, SizeComp>();
}